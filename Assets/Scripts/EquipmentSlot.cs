﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EquipmentSlot : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Tooltip tooltip;
    public CharacterPanel chP;

    public int slotId;

    public Item heldIt;
    public int invId;

    public Image itemVis;

    public string noItemMsg;
    public string itemMsg;

    void Awake()
    {
        tooltip = GetComponentInParent<Tooltip>();
        itemVis = transform.FindChild("Item").GetComponent<Image>();

        heldIt = new Item();
        ToggleItemVis(false, heldIt);
    }

    void OnEnable()
    {
        if(!chP && GameManager.instance.myPc)
        {
            chP = GameManager.instance.myPc.GetComponent<CharacterPanel>();
        }
    }

    public void OnPointerDown(PointerEventData data)
    {
        if(data.button == PointerEventData.InputButton.Right)
        {
            if(heldIt.itemId != -1)
            {
                chP.inv.DeequipItem(invId, slotId);
                tooltip.Deactivate();
            }
        }
    }

    public void OnPointerEnter(PointerEventData data)
    {
        if (heldIt.itemId != -1)
        {
            tooltip.Activate(heldIt, itemMsg);
        }
        else
        {
            tooltip.Activate(noItemMsg);
        }
    }

    public void OnPointerExit(PointerEventData data)
    {
        tooltip.Deactivate();
    }

    public void ToggleItemVis(bool b, Item it)
    {
        if(it.itemId != -1)
        {
            itemVis.overrideSprite = it.sprite;
        }

        itemVis.gameObject.SetActive(b);
    }
}
