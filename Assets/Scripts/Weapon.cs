﻿using UnityEngine;

[System.Serializable]
public class Weapon : Item
{
    public EWeaponType wepType;
    public float damage;
    public float range;
    public float cooldown;
    public float stagger;
    public float knockback;
    public float reqstamina;
    public float block;
    public int angle;

    public Weapon(int id, string name, EItemType type, EItemRarity rarity, EWeaponType wtype, float dmg, float rng, float cd, float stg,
        float kck, float reqst, float bck, int ag, int amount, string slug)
    {
        itemId = id;
        itemName = name;
        itemType = type;
        itemRarity = rarity;

        wepType = wtype;
        damage = dmg;
        range = rng;
        cooldown = cd;
        stagger = stg;
        knockback = kck;
        reqstamina = reqst;
        block = bck;
        angle = ag;

        itemAmount = amount;

        sprite = Resources.Load<Sprite>("Sprites/Items/" + itemType + "/" + slug);
        itemSlug = slug;
    }
}
