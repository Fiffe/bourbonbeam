﻿using UnityEngine;
using System.Collections;

public class IngameMenu : MonoBehaviour
{
    public bool activated;

    private Transform menu;

    void Start()
    {
        menu = transform.GetChild(0);
        ToggleMenu(false);
    }

    void Update()
    {
        if(Input.GetButtonDown("Menu") && !GameManager.instance.myInv.opened && !GameManager.instance.myChP.opened)
        {
            ToggleMenu(!activated);
        }
    }

    public void ToggleMenu(bool b)
    {
        activated = b;
        GameManager.instance.cursorVisible = b;
        menu.gameObject.SetActive(b);
    }

    public void BackToMainMenu()
    {
        GameManager.instance.levelMgr.BackToMenu();
    }
}