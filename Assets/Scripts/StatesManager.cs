﻿using UnityEngine;
using System.Collections;

public class StatesManager : MonoBehaviour
{
    private Animator anim;

    public bool canWalk;
    public bool canAttack;

    public bool isDead;
    public bool isWalking;
    public bool isHit;
    public bool isBlocking;

    public bool stagger;

    private float hitTimer;

    void Start()
    {
        anim = transform.FindChild("Sprite").GetComponent<Animator>();
    }

    void Update()
    {
        if(isHit)
        {
            hitTimer += Time.deltaTime;

            if(hitTimer >= 0.3f)
            {
                hitTimer = 0;
                isHit = false;
            }
        }

        if(stagger)
        {
            canAttack = false;
            canWalk = false;
        }
        
        if(!stagger && !isDead)
        {
            canAttack = true;
            canWalk = true;
        }

        HandleAnimator();
    }

    void HandleAnimator()
    {
        anim.SetBool("Walking", isWalking);
        anim.SetBool("Hit", stagger);
        anim.SetBool("Dead", isDead);
    }
}
