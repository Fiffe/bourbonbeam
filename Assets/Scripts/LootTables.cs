﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using LitJson;

public class LootTables : MonoBehaviour
{
    private List<LootTable> Database = new List<LootTable>();
    private JsonData tableData;

    void Start()
    {
        tableData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/LootTables.json"));
        CreateDatabase();
    }

    void CreateDatabase()
    {
        for (int i = 0; i < tableData.Count; i++)
        {
            LootDrop[] items = new LootDrop[tableData[i]["items"].Count];

            for(int k = 0; k < tableData[i]["items"].Count; k++)
            {
                items[k] = new LootDrop((int)tableData[i]["items"][k]["id"], (int)tableData[i]["items"][k]["amountMin"], 
                    (int)tableData[i]["items"][k]["amountMax"], float.Parse((string)tableData[i]["items"][k]["rate"]));
            }

            Database.Add(new LootTable((int)tableData[i]["id"], (string)tableData[i]["slug"], items));
        }
    }

    public LootTable GetTableById(int id)
    {
        for (int i = 0; i < Database.Count; i++)
        {
            if(id == Database[i].id)
            {
                return Database[i];
            }
        }

        return null;
    }
}

[System.Serializable]
public class LootTable
{
    public int id;
    public string slug;
    public LootDrop[] items;

    public LootTable(int i, string slg, LootDrop[] its)
    {
        id = i;
        slug = slg;
        items = its;
    }
}

public class LootDrop
{
    public int id;
    public int minAmt;
    public int maxAmt;
    public float rate;

    public LootDrop(int i, int min, int max, float r)
    {
        id = i;
        minAmt = min;
        maxAmt = max;
        rate = r;
    }
}