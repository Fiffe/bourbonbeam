﻿using UnityEngine;
using System.Collections.Generic;

public class Footsteps : MonoBehaviour
{
    public AudioSource src;
    public List<AudioClip> Steps = new List<AudioClip>();

    public void Step()
    {
        src.PlayOneShot(Steps[Random.Range(0, Steps.Count - 1)]);
    }
}
