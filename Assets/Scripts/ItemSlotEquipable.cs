﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemSlotEquipable : ItemSlot, IPointerClickHandler
{
    private bool isEquipped = false;
    private int equipSlot = -1;

    public void OnPointerClick(PointerEventData data)
    {
        if (data.button == PointerEventData.InputButton.Right)
        {
            if (it.itemType == EItemType.Weapon)
            {
                if (!isEquipped)
                {
                    equipSlot = inv.EquipItem(this);
                    if (equipSlot != -1)
                    {
                        ToggleEquip(true);
                    }
                }
                else
                {
                    inv.DeequipItem(slotId, equipSlot);
                }
            }
        }
    }

    public override void OnBeginDrag(PointerEventData data)
    {
        if (canDrag)
        {
            if (inv.opened)
            {
                transform.SetParent(outside);
                img.raycastTarget = false;

                if(isEquipped)
                {
                    inv.DeequipItem(slotId, equipSlot);
                }

                if (!loot)
                {
                    inv.Items[slotId] = new Item();
                    inv.Slots.Remove(gameObject);
                    inv.occupiedSlots--;
                }
            }
            else
            {
                ReturnItem();
            }
        }
    }

    public void ToggleEquip(bool b)
    {
        isEquipped = b;
        equipIcon.SetActive(b);
    }
}
