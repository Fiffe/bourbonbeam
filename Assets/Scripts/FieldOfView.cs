﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FieldOfView : MonoBehaviour
{
    public float viewRadius;

    [Range(0, 360)]
    public float viewAngle;

    public LayerMask targetMask;
    public LayerMask obstacleMask;

    public List<Transform> VisibleTargets = new List<Transform>();

    [Header("Editor")]
    public bool drawVisualization;

    void Start()
    {
        StartCoroutine("FindTargets", 0.2f);
    }

    public Vector3 AngleDir(float degrees, bool global)
    {
        if(!global)
        {
            degrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Cos(degrees * Mathf.Deg2Rad), Mathf.Sin(degrees * Mathf.Deg2Rad), 0);
    }

    void FindVisibleTargets()
    {
        VisibleTargets.Clear();
        Collider2D[] targetsInRadius = Physics2D.OverlapCircleAll(transform.position, viewRadius, targetMask);

        for(int i = 0; i < targetsInRadius.Length; i++)
        {
            Transform target = targetsInRadius[i].transform;
            float dist = Vector2.Distance(transform.position, target.position);

            if (!target.GetComponent<StatesManager>().isDead)
            {
                target.GetComponent<VisibilityHandler>().visible = false;

                Vector3 dir = (target.position - transform.position).normalized;

                if (dist > 1.75f)
                {
                    if (Vector3.Angle(-transform.parent.up, dir) < viewAngle / 2)
                    {
                        if (!Physics2D.Raycast(transform.position, dir, dist, obstacleMask))
                        {
                            VisibleTargets.Add(target);
                            target.GetComponent<VisibilityHandler>().visible = true;
                        }
                    }
                }
                else
                {                    
                    if (!Physics2D.Raycast(transform.position, dir, dist, obstacleMask))
                    {
                        VisibleTargets.Add(target);
                        target.GetComponent<VisibilityHandler>().visible = true;
                    }
                }
            }
        }
    }

    IEnumerator FindTargets(float delay)
    {
        while(true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }
}
