﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    public Transform target;
    public Transform shake;

    public float baseSpeed;

    private float smooth;

    public float shakeIntensity;

    public bool locked;

    void Start()
    {
        GameManager.instance.camMov = this;
    }

    void Update()
    {
        locked = GameManager.instance.cursorVisible;
    }

    void FixedUpdate()
    {
        FollowTarget();
    }

    void FollowTarget()
    {
        if (target != null)
        {
            smooth = 0.5f + (transform.position - target.position).sqrMagnitude;
            smooth = Mathf.Clamp(smooth, 0.5f, 1.5f);

            Vector3 pos;

            if (!locked)
            {
                pos = ((GameManager.instance.myPlayer.GetComponent<PlayerController>().crosshairPos - target.position) * 0.5f) + target.position;
            }
            else
            {
                pos = target.position;
            }

            float dst = Vector2.Distance(target.position, pos);
            if (dst > 2)
            {
                Vector3 vec = target.position - pos;
                vec = vec.normalized;
                vec *= dst - 2;
                pos += vec;
            }

            transform.position = Vector2.Lerp(transform.position, pos, baseSpeed * Time.deltaTime * smooth);
        }
        else
        {
            if (GameManager.instance.myPlayer != null)
            {
                target = GameManager.instance.myPlayer.transform;
            }
        }
    }

    public void Shake(float a, float t)
    {
        shakeIntensity = a;
        InvokeRepeating("StartShaking", 0, 0.1f);
        Invoke("StopShaking", t);
    }

    void StartShaking()
    {
        if (shakeIntensity > 0)
        {
            float quakeAmount = Random.Range(-0.1f, 0.1f) * shakeIntensity * 2;
            Vector2 pp = shake.position;
            pp.x += quakeAmount;
            pp.y += quakeAmount;
            shake.position = pp;
        }
    }

    void StopShaking()
    {
        CancelInvoke("StartShaking");
        shake.localPosition = new Vector2(0, 0);
    }
}