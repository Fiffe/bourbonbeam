﻿using UnityEngine;

[System.Serializable]
public class Item
{
    public int itemId;
    public string itemName = "";
    public EItemType itemType;
    public EItemRarity itemRarity;
    public int itemAmount;
    public string itemSlug;
    public Sprite sprite;

    public Item(int id, string name, EItemType type, EItemRarity rarity, int amount, string slug)
    {
        itemId = id;
        itemName = name;
        itemType = type;
        itemRarity = rarity;
        itemAmount = amount;

        sprite = Resources.Load<Sprite>("Sprites/Items/" + itemType + "/" + slug);
        itemSlug = slug;
    }

    public Item()
    {
        itemId = -1;
    }
}
