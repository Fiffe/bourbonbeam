﻿using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{
    private Inventory inv;
    private CharacterPanel chP;
    private GameObject tp;
    private RectTransform tpRect;
    public Text txt;

    void Awake()
    {
        tp = transform.FindChild("Tooltip").gameObject;
        tpRect = tp.GetComponent<RectTransform>();

        if (transform.name == "Inventory")
        {
            inv = GameManager.instance.myInv;
        }

        if(transform.name == "Character")
        {
            chP = GameManager.instance.myChP;
        }
                
        txt = tp.GetComponentInChildren<Text>();
    }

    void Start()
    {
        Deactivate();
    }

    void Update()
    {
        if (!inv || !chP)
        {
            if (transform.name == "Inventory")
            {
                inv = GameManager.instance.myInv;
            }

            if (transform.name == "Character")
            {
                chP = GameManager.instance.myChP;
            }
        }

        if (tp.activeSelf)
        {
            Vector3 pos = new Vector3(Input.mousePosition.x + 10, Input.mousePosition.y - 10);
            pos.z = 100;
            tp.transform.position = Vector3.Lerp(tp.transform.position, Camera.main.ScreenToWorldPoint(pos), Time.deltaTime * 8f);
        }

        if (inv)
        {
            if (tp.activeSelf && !inv.opened)
            {
                Deactivate();
            }
        }

        if(chP)
        {
            if (tp.activeSelf && !chP.opened)
            {
                Deactivate();
            }
        }
    }

    public void Activate(Item it)
    {
        Color32 c = RarityColors.GetRarityColor(it.itemRarity);
        string cs = ColorConverter.ColorToHex(c);

        txt.text = "<color='#" + cs + "'><b>" + it.itemName + "</b></color>\n" + it.itemType;

        if (it.itemType == EItemType.Weapon)
        {
            Weapon wep = (Weapon)it;

            txt.text += "\n\nDamage: <color=\"#ffffff\">" + wep.damage + "</color>\nHit/Sec: <color=\"#ffffff\">" + wep.cooldown + 
                "</color>\nStagger: <color=\"#ffffff\">" + wep.stagger + "</color>\nKnockback: <color=\"#ffffff\">" + wep.knockback +
                "</color>\nStamina: <color=\"#ffffff\">" + wep.reqstamina + "</color>\nBlock: <color=\"#ffffff\">" + wep.block + "</color>";
        }

        tp.SetActive(true);
    }

    public void Activate(Item it, string str)
    {
        Color32 c = RarityColors.GetRarityColor(it.itemRarity);
        string cs = ColorConverter.ColorToHex(c);
        txt.text = "<color='#" + cs + "'><b>" + it.itemName + "</b></color>\n\n" + it.itemType;

        if (it.itemType == EItemType.Weapon)
        {
            Weapon wep = (Weapon)it;

            txt.text += "\n\nDamage: <color=\"#ffffff\">" + wep.damage + "</color>\nHit/Sec: <color=\"#ffffff\">" + wep.cooldown +
                "</color>\nStagger: <color=\"#ffffff\">" + wep.stagger + "</color>\nKnockback: <color=\"#ffffff\">" + wep.knockback +
                "</color>\nStamina: <color=\"#ffffff\">" + wep.reqstamina + "</color>\nBlock: <color=\"#ffffff\">" + wep.block + "</color>";
        }

        txt.text += "\n\n" + str;

        tp.SetActive(true);
    }

    public void Activate(string str)
    {
        txt.text = str;
        tp.SetActive(true);
    }

    public void Deactivate()
    {
        tp.SetActive(false);
    }
}