﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class UIButtonText : MonoBehaviour
{
    public bool active;

    public List<Text> Texts = new List<Text>();
    public Color hoverColor;
    public Color pressColor;
    public Color disabledColor;
    public Color defaultColor;

    private float speed = 0.2f;
    private int selected = -1;
    private int previous = -1;
    private Color sC;

    public bool ready;

    void Start()
    {
        sC = hoverColor;
        Text[] txts = gameObject.GetComponentsInChildren<Text>();

        foreach(Text t in txts)
        {
            Texts.Add(t);
            t.CrossFadeColor(defaultColor, 0, true, true);
        }
    }

    void Update()
    {
        if (active)
        {
            if(Input.GetKeyDown(KeyCode.W))
            {
                previous = selected;
                selected--;
                Hover();
            }

            if(Input.GetKeyDown(KeyCode.S))
            {
                previous = selected;
                selected++;
                Hover();
            }

            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
            {
                if (ready)
                {
                    Press();
                    Texts[selected].GetComponentInParent<Button>().onClick.Invoke();
                }
            }

            selected = Mathf.Clamp(selected, 0, Texts.Count - 1);

            if (selected != -1)
            {
                if (previous != -1)
                {
                    Texts[previous].GetComponent<Outline>().enabled = false;
                    Texts[previous].CrossFadeColor(defaultColor, 0.2f, true, true);
                    //Texts[previous].fontStyle = FontStyle.Normal;
                }
                Texts[selected].GetComponent<Outline>().enabled = true;
                Texts[selected].CrossFadeColor(sC, speed, true, true);
                //Texts[selected].fontStyle = FontStyle.Bold;
            }
        }
    }

    public void Hover(int id)
    {
        previous = selected;
        selected = id;
        sC = hoverColor;
    }

    void Hover()
    {
        sC = hoverColor;
    }

    public void Press()
    {
        StartCoroutine("Pressing");
    }

    IEnumerator Pressing()
    {
        speed = 0.1f;
        sC = pressColor;
        Texts[selected].rectTransform.localScale = new Vector3(0.95f, 0.95f, 1);
        yield return new WaitForSeconds(0.2f);
        Texts[selected].rectTransform.localScale = new Vector3(1f, 1f, 1);
        sC = hoverColor;
        speed = 0.2f;
    }
}
