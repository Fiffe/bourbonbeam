﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public Tooltip tp;
    public Item it;

    public int slotId;
    public int amount;

    public Image img;

    protected Inventory inv;

    public Transform content;
    protected Transform outside;

    protected GameObject equipIcon;

    public bool canDrag = true;
    public bool loot = false;

    public LootableItem lootIt;

    void Awake()
    {
        img = GetComponent<Image>();
        inv = GameManager.instance.myInv;
        content = transform.parent;
        outside = transform.parent.parent.parent.parent.parent.parent;
        equipIcon = transform.FindChild("Equip").gameObject;

        equipIcon.SetActive(false);
    }

    void Update()
    {
        if (!inv.opened && !img.raycastTarget)
        {
            GetComponent<Image>().raycastTarget = true;
        }
    }

    public void OnPointerEnter(PointerEventData data)
    {
        tp.Activate(it);
    }

    public void OnPointerExit(PointerEventData data)
    {
        tp.Deactivate();
    }

    public virtual void OnBeginDrag(PointerEventData data)
    {
        if(canDrag)
        {
            if(inv.opened)
            {
                transform.SetParent(outside);
                img.raycastTarget = false;

                if(!loot)
                {
                    inv.Items[slotId] = new Item();
                    inv.Slots.Remove(gameObject);
                    inv.occupiedSlots--;
                }
            }
            else
            {
                ReturnItem();
            }
        }
    }

    public void OnDrag(PointerEventData data)
    {
        if(canDrag)
        {
            if(inv.opened)
            {
                Vector3 sp = Input.mousePosition + new Vector3(16, -32);
                sp.z = 100;
                transform.position = Camera.main.ScreenToWorldPoint(sp);
            }
            else
            {
                ReturnItem();
            }
        }
        else
        {
            ReturnItem();
        }
    }

    public void OnEndDrag(PointerEventData data)
    {
        if(canDrag)
        {
            ReturnItem();
        }
    }

    protected void ReturnItem()
    {
        transform.SetParent(content);
        img.raycastTarget = true;

        if (!loot)
        {
            inv.Items[slotId] = it;
            inv.Slots.Add(gameObject);
            inv.occupiedSlots++;
        }
    }
}
