﻿using UnityEngine;
using System.Collections;

public class LootableChar : Lootable
{
    protected CharacterStats stats;
    protected bool activate = false;

    void Awake()
    {
        base.Awake();

        stats = GetComponent<CharacterStats>();
    }

    void Update()
    {
        if(stats.isDead && !activate)
        {
            RollItems();
            if (Loot.Count > 0)
            {
                ToggleEffect(true);
            }
            else
            {
                GetComponent<BoxCollider2D>().enabled = false;
            }

            activate = true;
        }

        base.Update();
    }
}
