﻿using UnityEngine;
using System.Collections;

public class PropDestroyable : MonoBehaviour
{
    public bool broken;

    private SpriteRenderer rend;
    private CircleCollider2D col;

    private Sprite brokenSprite;

    public GameObject[] effects;

    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        col = GetComponent<CircleCollider2D>();
        brokenSprite = Resources.Load<Sprite>("Sprites/Props/" + rend.sprite.name + "_Broken");
    }

    public void Damage()
    {
        rend.sprite = brokenSprite;
        col.enabled = false;

        if(transform.childCount > 0)
        {
            foreach(Transform tr in transform)
            {
                tr.gameObject.SetActive(false);
            }
        }

        rend.sortingLayerName = "Corpses";
        GetComponent<SpriteOrderFix>().enabled = false;

        if(effects.Length > 0)
        {
            foreach(GameObject ps in effects)
            {
                GameObject go = Instantiate(ps, transform.position, Quaternion.identity) as GameObject;
                go.GetComponent<ParticleSystem>().Play();
                go.AddComponent<Lifetime>();
                go.GetComponent<Lifetime>().lifetime = go.GetComponent<ParticleSystem>().startLifetime;
            }
        }
    }
}
