﻿public enum EItemRarity
{
    Common,
    Rare,
    Legendary,
    Mythical
}