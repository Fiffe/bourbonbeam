﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponSlashing : WeaponBase
{
    private LayerMask enemyMask;
    private LayerMask obstaclesMask;

    public float angle;

    public List<Transform> EnemiesToHit = new List<Transform>();

    [Header("Visualization")]
    public bool drawVisualization;

    public void Start()
    {
        base.Start();
        if (owner.layer == LayerMask.NameToLayer("Enemy"))
        {
            enemyMask = (1 << LayerMask.NameToLayer("Friendly"));
        }
        else
        {
            enemyMask = (1 << LayerMask.NameToLayer("Enemy") | 1 << LayerMask.NameToLayer("Props"));
        }

        obstaclesMask = (1 << LayerMask.NameToLayer("Terrain"));

        StartCoroutine("GetTargetsDelayed", 0.2f);
    }

    public Vector3 AngleDir(float degrees, bool global)
    {
        if (!global)
        {
            degrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(degrees * Mathf.Deg2Rad), Mathf.Cos(degrees * Mathf.Deg2Rad), 0);
    }

    void GetTargets()
    {
        EnemiesToHit.Clear();
        Collider2D[] targetsInRadius = Physics2D.OverlapCircleAll(transform.position, wepIt.range, enemyMask);

        for (int i = 0; i < targetsInRadius.Length; i++)
        {
            Transform target = targetsInRadius[i].transform;
            Vector3 dir = (target.position - transform.transform.position).normalized;

            if (target.gameObject.layer == LayerMask.NameToLayer("Enemy") || target.gameObject.layer == LayerMask.NameToLayer("Friendly"))
            {
                if (!target.GetComponent<StatesManager>().isDead)
                {
                    if (Vector3.Angle(-holder.up, dir) < angle)
                    {
                        float dist = Vector2.Distance(transform.position, target.position);

                        Debug.DrawRay(transform.position, dir * dist, Color.yellow);

                        if (!Physics2D.Raycast(transform.position, dir, dist, obstaclesMask))
                        {
                            EnemiesToHit.Add(target);
                        }
                    }
                }
            }
            else if(target.gameObject.layer == LayerMask.NameToLayer("Props"))
            {
                if (target.GetComponent<PropDestroyable>() && !target.GetComponent<PropDestroyable>().broken)
                {
                    if (Vector3.Angle(-holder.up, dir) < angle)
                    {
                        float dist = Vector2.Distance(transform.position, target.position);

                        Debug.DrawRay(transform.position, dir * dist, Color.yellow);

                        if (!Physics2D.Raycast(transform.position, dir, dist, obstaclesMask))
                        {
                            EnemiesToHit.Add(target);
                        }
                    }
                }
            }
        }
    }

    public override void Use()
    {
        if (!isBlocking)
        {
            base.Use();

            HitTargets();
        }
    }

    void HitTargets()
    {
        foreach(Transform tr in EnemiesToHit)
        {
            if(tr.GetComponent<CharacterStats>())
            {
                StatesManager man = tr.GetComponent<StatesManager>();
                CharacterStats estats = tr.GetComponent<CharacterStats>();

                float dmg = wepIt.damage + stats.bonusDmg;
                float kck = wepIt.knockback;

                if(stats.stamina < wepIt.reqstamina)
                {
                    dmg /= 2;
                    kck /= 2;
                }

                bool canHit = true;

                if (man.isBlocking && estats.stamina > dmg)
                {
                    Vector3 dir = (tr.position - owner.transform.position).normalized;
                    if (Vector3.Dot(dir, -tr.FindChild("Weapon").up) > 0)
                    {
                        canHit = true;
                    }
                    else
                    {
                        canHit = false;
                    }
                }

                estats.GetHit(tr.position - owner.transform.position, dmg, wepIt.stagger, kck, owner, canHit);

                if (canHit)
                {
                    PlaySound(HitSounds[Random.Range(0, HitSounds.Count - 1)]);

                    GameManager.instance.decMan.SpawnDecal(new Vector3(tr.position.x + Random.Range(-0.3f, 0.3f),
                    tr.position.y + Random.Range(-0.6f, 0.3f), tr.position.z), estats.decGroupId);
                }
                else
                {
                    PlaySound(BlockedSounds[Random.Range(0, BlockedSounds.Count - 1)]);
                }
            }

            if(tr.GetComponent<PropDestroyable>())
            {
                PropDestroyable prop = tr.GetComponent<PropDestroyable>();
                prop.Damage();
                PlaySound(HitSounds[Random.Range(0, HitSounds.Count - 1)]);
            }
        }

        if (EnemiesToHit.Count > 0)
        {
            if (owner.tag == "Player")
            {
                GameManager.instance.ShakeScreen(0.15f, .1f);
            }
        }
    }

    IEnumerator GetTargetsDelayed(float delay)
    {
        while(true)
        {
            yield return new WaitForSeconds(delay);
            GetTargets();
        }
    }
}
