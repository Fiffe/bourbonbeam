﻿using UnityEngine;
using System.Collections;

public class DynamicFlare : MonoBehaviour
{
    public float speed = 5;
    public float scaleRangeMin = 1;
    public float scaleRangeMax = 1;

    public Light light;
    public float intensityBase = 5;

    void Start()
    {
        light = GetComponentInChildren<Light>();
    }

    void Update()
    {
        float r = Random.Range(scaleRangeMin, scaleRangeMax);
        Vector3 v = new Vector3(r, r, 1);
        transform.localScale = Vector3.Lerp(transform.localScale, v, Time.deltaTime * speed);

        float r2 = r * intensityBase * 2.5f;
        light.intensity = Mathf.Lerp(light.intensity, r2, Time.deltaTime * speed);
    }
}
