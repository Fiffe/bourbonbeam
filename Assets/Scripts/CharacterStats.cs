﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterStats : MonoBehaviour
{
    private StatesManager states;
    private Animator anim;
    private Rigidbody2D rb;

    [Header("Character Stats")]
    public Stat strength = new Stat("STR", 1, "<color='#ffffff'>STRENGTH</color> increases your health, it also helps you use heavier weapons more efficently and increases your weapon's knockback.");
    public Stat agility = new Stat("AGI", 1, "<color='#ffffff'>AGILITY</color> increases the bonus damage dealt by your character.");
    public Stat endurance = new Stat("END", 1, "<color='#ffffff'>ENDURANCE</color> increases your stamina.");
    public Stat intelligence = new Stat("INT", 1, "<color='#ffffff'>INTELLIGENCE</color> increases mana and lets you use more advanced spells.");
    public Stat luck = new Stat("LCK", 1, "<color='#ffffff'>LUCK</color> increases your chance for critical hit.");
    public List<Stat> Stats = new List<Stat>();

    [Header("Progression")]
    public int level = 1;
    public float exp = 0;
    public float reqExp;
    public int lp = 0;
    public int lpPerLvl = 3;
    public float expConst = 5f;
    public float expWorth = 1;

    [Header("Metabolism")]
    public float health = 100;
    public float maxHealth;
    public float baseHealth = 100;
    public float mana = 100;
    public float maxMana;
    public float baseMana = 100;
    public float stamina = 100;
    public float maxStamina;
    public float baseStamina = 100;

    [Header("Bonuses")]
    public float bonusDmg = 0;
    public float bonusCrit = 0;
    public float bonusKnock = 0;

    public SpriteRenderer baseSprite;

    private float staggerTime;
    private float staggerTimer;
    private bool stagger;

    [Header("Defence")]
    public float blockedDamage = 0;
    public float staggerCdTime;
    private float staggerCdTimer;
    private bool staggerCooldown;

    [Header("Stamina Rates")]
    private bool justUsedStamina;
    public float staminaRegenCd = 3;
    public float staminaRegenRate = 1;
    private float staminaRegenTimer = 0;
    private float timeSinceStaminaUsed = 0;

    [Header("Effects")]
    public Transform effectsHolder;
    public List<ParticleSystem> Effects = new List<ParticleSystem>();
    public int decGroupId = 0;

    private bool flash;

    public bool isDead;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        states = GetComponent<StatesManager>();
        baseSprite = transform.FindChild("Sprite").GetComponentInChildren<SpriteRenderer>();
        anim = baseSprite.transform.parent.GetComponent<Animator>();

        effectsHolder = transform.FindChild("Effects");

        LoadStats();

        foreach (Transform tr in effectsHolder)
        {
            if (tr.GetComponent<ParticleSystem>())
            {
                Effects.Add(tr.GetComponent<ParticleSystem>());
            }
        }
    }

    void Start ()
    {
        RecalculateStats();
        RecalculateLevel();

        if (mana > maxMana)
        {
            mana = maxMana;
        }

        if (stamina > maxStamina)
        {
            stamina = maxStamina;
        }

        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }

    void LoadStats()
    {
        Stats.Add(strength);
        Stats.Add(agility);
        Stats.Add(endurance);
        Stats.Add(intelligence);
        Stats.Add(luck);
    }

	void Update ()
    {
        HandleLife();

        ControlFlashing();
        ControlStagger();

        if (!isDead)
        {            
            HandleStamina();
        }

        if(exp >= reqExp)
        {
            float left = exp - reqExp;
            LevelUp();
            exp += left;
        }
    }

    void ControlFlashing()
    {
        float v;

        if (flash)
        {
            v = Mathf.Lerp(baseSprite.material.GetFloat("_FlashAmount"), .7f, Time.deltaTime * 25);
        }
        else
        {
            v = Mathf.Lerp(baseSprite.material.GetFloat("_FlashAmount"), 0f, Time.deltaTime * 25);
        }

        baseSprite.material.SetFloat("_FlashAmount", v);
        baseSprite.material.SetFloat("_SelfIllum", v);
    }

    void ControlStagger()
    {
        if(stagger && !staggerCooldown)
        {
            staggerTimer += Time.deltaTime;
            
            if(staggerTimer >= staggerTime)
            {
                staggerTimer = 0;
                stagger = false;
                staggerCooldown = true;
            }
        }

        if(staggerCooldown)
        {
            stagger = false;
            staggerTimer = 0;

            staggerCdTimer += Time.deltaTime;
            if(staggerCdTimer >= staggerCdTime)
            {
                staggerCdTimer = 0;
                staggerCooldown = false;
            }
        }

        states.stagger = this.stagger;
    }

    void RecalculateStats()
    {
        maxHealth = baseHealth + (10 * strength.level);
        maxMana = baseMana + (10 * intelligence.level);
        maxStamina = baseStamina + (10 * endurance.level);

        bonusDmg = agility.level * 0.5f;
        bonusCrit = luck.level * 0.05f;
        bonusKnock = strength.level * 0.1f;
    }

    void RecalculateLevel()
    {
        reqExp = 100 * 2^((level + 1)-2);
    }

    void LevelUp()
    {
        lp += lpPerLvl;
        exp = 0;
        level++;
        RecalculateLevel();
    }

    void HandleLife()
    {
        states.isDead = isDead;

        if (health <= 0 && !isDead)
        {
            Die();
        }
        else if(isDead && health > 0)
        {
            ToggleObjects(true);
            ChangeSpriteLayer(true);
            isDead = false;
        }

        health = Mathf.Clamp(health, 0, maxHealth);
    }

    void HandleStamina()
    {
        timeSinceStaminaUsed += Time.deltaTime;
        timeSinceStaminaUsed = Mathf.Clamp(timeSinceStaminaUsed, 4, 15f);

        if (!justUsedStamina)
        {
            stamina += staminaRegenRate * timeSinceStaminaUsed/10 * Time.deltaTime;
        }
        else
        {
            staminaRegenTimer += Time.deltaTime;

            if(staminaRegenTimer >= staminaRegenCd)
            {
                justUsedStamina = false;
                staminaRegenTimer = 0;
            }
        }

        stamina = Mathf.Clamp(stamina, 0, maxStamina);
    }

    void Die()
    {
        isDead = true;
        ToggleObjects(false);
        ChangeSpriteLayer(false);
    }

    void ToggleObjects(bool b)
    {
        transform.Find("Weapon").gameObject.SetActive(b);
        GetComponent<BoxCollider2D>().isTrigger = !b;
        GetComponentInChildren<CircleCollider2D>().isTrigger = !b;
    }

    void ChangeSpriteLayer(bool b)
    {
        if(b)
        {
            baseSprite.sortingLayerName = "Character";
        }
        else
        {
            baseSprite.sortingLayerName = "Corpses";
        }
    }

    public void GetHit(Vector2 dir, float dmg = 0, float stg = 0, float knockback = 0, GameObject who = null, bool canHit = true)
    {
        if(!canHit)
        {
            if(stamina < dmg)
            {
                canHit = true;
                UseStamina(dmg);
            }
        }

        if (canHit)
        {
            PlayAllEffects();
            health -= dmg;
            anim.SetTrigger("GotHit");
            Stagger(stg);
            Knockback(knockback, dir);
            StartCoroutine("FlashSprite", 0.075f);
            states.isHit = true;

            if(states.isBlocking)
            {
                UseStamina(dmg);
            }
        }
        else
        {
            GameManager.instance.ShakeScreen(0.15f, .1f);
            PlayEffect("Dust");
            Knockback(knockback/2, dir);
            float realDmg = dmg;
            realDmg = dmg - blockedDamage;
            realDmg = Mathf.Clamp(realDmg, 0, Mathf.Infinity);
            UseStamina(dmg);
            health -= realDmg;
        }

        if(health <= 0 && who)
        {
            who.GetComponentInChildren<CharacterStats>().exp += expWorth;
        }
    }

    public void UseStamina(float amt)
    {
        stamina -= amt;
        justUsedStamina = true;
        timeSinceStaminaUsed = 0;
    }

    void PlayAllEffects()
    {
        foreach(ParticleSystem ps in Effects)
        {
            ps.Play();
        }
    }

    void PlayEffect(string s)
    {
        foreach (ParticleSystem ps in Effects)
        {
            if(ps.name == s)
            {
                ps.Play();
            }
        }
    }

    public void Stagger(float t)
    {
        stagger = true;
        staggerTime = t;
    }

    public void Knockback(float power, Vector2 d)
    {
        rb.AddForce(d.normalized * 10000 * power);
    }

    IEnumerator FlashSprite(float d)
    {
        flash = true;
        yield return new WaitForSeconds(d);
        flash = false;
    }
}

[System.Serializable]
public class Stat
{
    public string name;
    public int level;
    public string description;

    public Stat(int lvl)
    {
        level = lvl;
    }

    public Stat(string n, int lvl, string d)
    {
        name = n;
        level = lvl;
        description = d;
    }
}