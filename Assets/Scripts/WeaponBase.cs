﻿using UnityEngine;
using System.Collections.Generic;

public class WeaponBase : MonoBehaviour
{
    public Weapon wepIt;

    public bool secondary;

    public bool canBlock;
    public bool isBlocking;

    public bool justUsed;

    protected float useTimer;

    protected Animator anim;
    protected GameObject owner;
    protected CharacterStats stats;

    protected Transform holder;

    public AudioSource audioSrc;

    public Vector3 equipPos;
    public Vector3 equipRot;

    public List<AudioClip> HitSounds = new List<AudioClip>();
    public List<AudioClip> SwingSounds = new List<AudioClip>();
    public List<AudioClip> BlockedSounds = new List<AudioClip>();

    public void Start()
    {
        wepIt = (Weapon)(GameManager.instance.itemDb.GetItemBySlug(gameObject.name));
        owner = transform.parent.parent.gameObject;
        holder = owner.transform.FindChild("Weapon");
        anim = holder.GetComponentInChildren<Animator>();
        stats = GetComponentInParent<CharacterStats>();
        audioSrc = GetComponentInChildren<AudioSource>();

        LoadSounds();
    }

    void LoadSounds()
    {
        AudioClip[] acs = Resources.LoadAll<AudioClip>("Sounds/Hit/");
        foreach (AudioClip ac in acs)
        {
            HitSounds.Add(ac);
        }

        acs = Resources.LoadAll<AudioClip>("Sounds/Swing/");

        foreach (AudioClip ac in acs)
        {
            SwingSounds.Add(ac);
        }

        acs = Resources.LoadAll<AudioClip>("Sounds/Blocked/");

        foreach (AudioClip ac in acs)
        {
            BlockedSounds.Add(ac);
        }
    }

    void Update()
    {
        if(justUsed)
        {
            useTimer += Time.deltaTime;
            if(useTimer >= wepIt.cooldown)
            {
                justUsed = false;
                useTimer = 0;
            }
        }

        anim.SetBool("Secondary", isBlocking);
    }

    public virtual void Use()
    {
        stats.UseStamina(wepIt.reqstamina);
        anim.SetTrigger("Use");
        PlaySound(SwingSounds[Random.Range(0, SwingSounds.Count - 1)]);
        justUsed = true;
    }

    public virtual void Secondary()
    {
        if (canBlock && !justUsed)
        {
            Block();
        }
    }

    void Block()
    {
        isBlocking = true;
    }

    public void PlaySound(AudioClip s)
    {
        audioSrc.PlayOneShot(s);
    }
}
