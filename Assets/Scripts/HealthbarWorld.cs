﻿using UnityEngine;
using System.Collections;

public class HealthbarWorld : MonoBehaviour
{
    private Transform healthbar;
    private Transform healthbarFill;

    private CharacterStats stats;

    float mappedHp = 1;

    void Start()
    {
        healthbar = transform.FindChild("Healthbar");
        healthbarFill = healthbar.GetChild(0);
        stats = GetComponent<CharacterStats>();
    }

    void Update()
    {
        HandleHealthbar();

        if(stats.isDead)
        {
            healthbar.gameObject.SetActive(false);
        }
    }

    void HandleHealthbar()
    {
        if (healthbar != null)
        {
            mappedHp = Mathf.Lerp(mappedHp, stats.health / stats.maxHealth, Time.deltaTime * 8f);
            mappedHp = Mathf.Clamp01(mappedHp);

            healthbarFill.localScale = new Vector3(mappedHp, 1, 1);
        }
    }
}
