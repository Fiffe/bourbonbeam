﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class CharacterPanel : MonoBehaviour
{
    [Header("Scripts")]
    public CharacterStats stats;
    public Inventory inv;
    public Tooltip tooltip;

    [Space(10)]
    public GameObject panel;

    [Header("Slots")]
    public EquipmentSlot hand;
    public EquipmentSlot offhand;

    private Transform left;
    private Transform right;

    private GameObject statTxtPrefab;
    private GameObject statTxtUpPrefab;

    private int spentLP = 0;

    [Header("Stats Text")]
    public List<StatText> StatsTxt = new List<StatText>();
    public Text bnsDmg;
    public Text bnsKck;
    public Text lvl;
    public Text exp;
    public Text rexp;
    public Text lp;
    public Text hp;
    public Text mn;
    public Text st;

    [Space(10)]
    public bool opened = true;

    private int prevLvl;
    private bool buttonsVisible;

    void Awake()
    {
        stats = GetComponent<CharacterStats>();
        inv = GetComponent<Inventory>();
        panel = GameManager.instance.HUD.transform.FindChild("Character").gameObject;

        statTxtPrefab = Resources.Load<GameObject>("Prefabs/Other/StatTxt");
        statTxtUpPrefab = Resources.Load<GameObject>("Prefabs/Other/StatTxtUp");

        panel.transform.FindChild("Layout").FindChild("Char").FindChild("CloseBtn").GetComponent<Button>().onClick.AddListener(() => CloseCharPanel());

        hand = panel.transform.FindChild("Layout").FindChild("Char").FindChild("Slots").FindChild("HandSlot").GetComponent<EquipmentSlot>();
        offhand = hand.transform.parent.FindChild("OffhandSlot").GetComponent<EquipmentSlot>();

        left = panel.transform.FindChild("Layout").FindChild("Char").FindChild("Stats").FindChild("Left");
        right = left.parent.FindChild("Right");

        bnsDmg = left.FindChild("BnsDmg").GetComponent<Text>();
        bnsKck = left.FindChild("BnsKck").GetComponent<Text>();
        lvl = right.FindChild("Lvl").GetComponent<Text>();
        exp = right.FindChild("Exp").GetComponent<Text>();
        rexp = right.FindChild("Rexp").GetComponent<Text>();
        lp = right.FindChild("Lp").GetComponent<Text>();
        hp = right.FindChild("Hp").GetComponent<Text>();
        mn = right.FindChild("Mn").GetComponent<Text>();
        st = right.FindChild("St").GetComponent<Text>();

        tooltip = panel.GetComponent<Tooltip>();

        CreateStatsVisuals();

        GameManager.instance.myChP = this;
    }

    void CreateStatsVisuals()
    {
        for (int i = stats.Stats.Count - 1; i >= 0; i--)
        {
            int index = stats.Stats.Count - 1 - i;
            GameObject statTxt = Instantiate(statTxtUpPrefab);
            StatText stt = new StatText(statTxt.GetComponent<Text>(), stats.Stats[i]);
            stt.txtGO.GetComponent<TextExtended>().tp = tooltip;
            stt.txtGO.GetComponent<TextExtended>().tooltipText = stt.heldStat.description;
            statTxt.name = stt.heldStat.name;
            statTxt.transform.SetParent(left, false);
            statTxt.transform.SetAsFirstSibling();
            statTxt.transform.FindChild("Plus").GetComponent<Button>().onClick.AddListener(() => UpgradeStat(index));
            statTxt.transform.FindChild("Minus").GetComponent<Button>().onClick.AddListener(() => DecreaseStat(index));

            StatsTxt.Add(stt);
        }

        ToggleUpgradeButtons(false);
    }

    void Start()
    {
        RefreshStats();
        TogglePanel(false);
    }

    void Update()
    {
        ControlInput();

        if(stats.lp > 0)
        {
            if(!buttonsVisible)
            {
                ToggleUpgradeButtons(true);
            }
        }

        if (stats.lp <= 0)
        {
            if (buttonsVisible)
            {
                if (spentLP <= 0)
                {
                    ToggleUpgradeButtons(true);
                }
            }
        }

        if(opened)
        {
            if(stats.level != prevLvl)
            {
                RefreshStats();
            }

            RefreshDynamic();
        }
    }

    void RefreshStatsTxt()
    {
        foreach (StatText stt in StatsTxt)
        {
            stt.txtGO.text = stt.heldStat.name.ToUpper() + ": <color=\"#ffffff\">" + stt.heldStat.level.ToString() + "</color>";
            stt.prevValue = stt.heldStat.level;
            stt.newValue = stt.heldStat.level;
        }
    }

    void RefreshStats()
    {
        RefreshStatsTxt();        

        lvl.text = "LEVEL: <color=\"#ffffff\">" + stats.level.ToString() + "</color>";
        rexp.text = "REXP: <color=\"#ffffff\">" + stats.reqExp.ToString() + "</color>";

        prevLvl = stats.level;
    }

    void RefreshDynamic()
    {
        bnsDmg.text = "BONUS DMG: <color=\"#ffffff\">" + stats.bonusDmg.ToString() + "</color>";
        bnsKck.text = "BONUS KNOCK: <color=\"#ffffff\">" + stats.bonusKnock.ToString() + "</color>";
        exp.text = "EXP: <color=\"#ffffff\">" + stats.exp.ToString() + "</color>";
        float s = stats.stamina;
        hp.text = "HEALTH: <color=\"#ffffff\">" + stats.health.ToString() + "/" + stats.maxHealth.ToString() + "</color>";
        mn.text = "MANA: <color=\"#ffffff\">" + stats.mana.ToString() + "/" + stats.maxMana.ToString() + "</color>";
        st.text = "STAMINA: <color=\"#ffffff\">" + s.ToString("F1") + "/" + stats.maxStamina.ToString() + "</color>";
        lp.text = "LP: <color=\"#ffffff\">" + stats.lp.ToString() + "</color>";
    }

    void ControlInput()
    {
        if (Input.GetButtonDown("Character") && !GameManager.instance.igMenu.activated && !GameManager.instance.myInv.opened)
        {
            TogglePanel(!opened);
        }

        if (Input.GetButtonDown("Cancel") && opened)
        {
            TogglePanel(false);
        }
    }

    void TogglePanel(bool b)
    {
        opened = b;
        GameManager.instance.cursorVisible = b;

        tooltip.Deactivate();

        if(b)
        {
            RefreshStats();
        }
        else
        {
            if(spentLP > 0)
            {
                stats.lp += spentLP;
                spentLP = 0;
            }
        }

        panel.SetActive(b);
    }

    void ToggleUpgradeButtons(bool b)
    {
        foreach(StatText stt in StatsTxt)
        {
            foreach(Transform tr in stt.txtGO.transform)
            {
                tr.gameObject.SetActive(b);
            }
        }

        buttonsVisible = b;
    }

    public void UpgradeStat(int id)
    {
        if (stats.lp > 0)
        {
            StatText stt = StatsTxt[id];
            Debug.Log(stt.heldStat.name);
            stt.newValue++;
            stt.txtGO.text = stt.heldStat.name.ToUpper() + ": <color=\"#ffffff\">" + stt.newValue.ToString() + "</color>";
            spentLP++;
            stats.lp--;
        }
    }

    public void DecreaseStat(int id)
    {
        if(StatsTxt[id].prevValue != StatsTxt[id].newValue)
        {
            StatText stt = StatsTxt[id];
            stt.newValue--;
            stt.txtGO.text = stt.heldStat.name.ToUpper() + ": <color=\"#ffffff\">" + stt.newValue.ToString() + "</color>";
            spentLP--;
            stats.lp++;
        }
    }

    public void CloseCharPanel()
    {
        TogglePanel(false);
    }
}

[System.Serializable]
public class StatText
{
    public Text txtGO;
    public Stat heldStat;
    public int prevValue;
    public int newValue;

    public StatText(Text txt, Stat st)
    {
        txtGO = txt;
        heldStat = st;
    }
}