﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class TextExtended : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Tooltip tp;

    [TextArea(3, 10)]
    public string tooltipText;

    void Awake()
    {
        tp = GetComponentInParent<Tooltip>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tp.Activate(tooltipText);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tp.Deactivate();
    }
}
