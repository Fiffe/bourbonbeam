﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using LitJson;

public class ItemDatabase : MonoBehaviour
{
    private List<Item> Database = new List<Item>();
    private JsonData itemData;

    void Start()
    {
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
        CreateDatabase();
    }

    void CreateDatabase()
    {
        for(int i = 0; i < itemData.Count; i++)
        {
            string typeData = (string)itemData[i]["type"];
            EItemType type = (EItemType)Enum.Parse(typeof(EItemType), typeData);

            string rarityData = (string)itemData[i]["rarity"];
            EItemRarity rarity = (EItemRarity)Enum.Parse(typeof(EItemRarity), rarityData);

            if (type == EItemType.Generic)
            {
                Database.Add(new Item((int)itemData[i]["id"], (string)itemData[i]["name"], type, rarity, (int)itemData[i]["amount"], (string)itemData[i]["slug"]));
            }
            else if(type == EItemType.Weapon)
            {
                string wepTypeData = (string)itemData[i]["weptype"];
                EWeaponType wepType = (EWeaponType)Enum.Parse(typeof(EWeaponType), wepTypeData);

                Database.Add(new Weapon((int)itemData[i]["id"], (string)itemData[i]["name"], type, rarity, wepType, float.Parse((string)itemData[i]["stats"]["damage"]),
                float.Parse((string)itemData[i]["stats"]["range"]), float.Parse((string)itemData[i]["stats"]["cooldown"]),
                float.Parse((string)itemData[i]["stats"]["stagger"]), float.Parse((string)itemData[i]["stats"]["knockback"]),
                float.Parse((string)itemData[i]["stats"]["reqstamina"]), float.Parse((string)itemData[i]["stats"]["block"]),
                (int)itemData[i]["stats"]["angle"], 1, (string)itemData[i]["slug"]));
            }
            else if(type == EItemType.Gold)
            {
                Database.Add(new Item((int)itemData[i]["id"], (string)itemData[i]["name"], type, rarity, (int)itemData[i]["amount"], (string)itemData[i]["slug"]));
            }
        }
    }

    public Item GetItemById(int id)
    {
        for(int i = 0; i < Database.Count; i++)
        {
            if(Database[i].itemId == id)
            {
                return Database[i];
            }
        }

        return null;
    }

    public Item GetItemBySlug(string slug)
    {
        for (int i = 0; i < Database.Count; i++)
        {
            if (Database[i].itemSlug == slug)
            {
                return Database[i];
            }
        }

        return null;
    }
}
