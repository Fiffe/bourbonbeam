﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(WeaponSlashing))]
public class WeaponSlashingEditor : Editor
{
    void OnSceneGUI()
    {
        WeaponSlashing wp = (WeaponSlashing)target;

        if (wp.drawVisualization && wp.transform)
        {
            Handles.color = Color.white;
            Handles.DrawWireArc(wp.transform.position, Vector3.forward, Vector3.up, 360, wp.wepIt.range);
            Vector3 viewAngleA = wp.AngleDir(-wp.angle / 2, false);
            Vector3 viewAngleB = wp.AngleDir(wp.angle / 2, false);

            Handles.DrawLine(wp.transform.position, wp.transform.position + viewAngleA * wp.wepIt.range);
            Handles.DrawLine(wp.transform.position, wp.transform.position + viewAngleB * wp.wepIt.range);

            foreach (Transform tr in wp.EnemiesToHit)
            {
                Handles.DrawLine(wp.transform.position, tr.position);
            }
        }
    }
}
