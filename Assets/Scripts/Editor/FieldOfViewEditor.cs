﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(FieldOfView))]
public class FieldOfViewEditor : Editor
{
    void OnSceneGUI()
    {
        FieldOfView fov = (FieldOfView)target;

        if (fov.drawVisualization)
        {
            Handles.color = Color.white;
            Handles.DrawWireArc(fov.transform.position, Vector3.forward, Vector3.up, 360, fov.viewRadius);
            Vector3 viewAngleA = fov.AngleDir(-fov.viewAngle / 2, false);
            Vector3 viewAngleB = fov.AngleDir(fov.viewAngle / 2, false);

            Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngleA * fov.viewRadius);
            Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngleB * fov.viewRadius);

            foreach (Transform tr in fov.VisibleTargets)
            {
                Handles.DrawLine(fov.transform.position, tr.position);
            }
        }
    }
}
