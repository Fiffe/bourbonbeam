﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class DecalManager : MonoBehaviour
{
    public Transform decalsParent;
    public List<GameObject> CreatedDecals = new List<GameObject>();

    public List<DecalGroup> AvailableDecals = new List<DecalGroup>();

    protected GameObject decalPrefab;

    void Awake()
    {
        decalPrefab = Resources.Load<GameObject>("Prefabs/Effects/Decal");
    }

    void Start()
    {
        decalsParent = new GameObject().transform;
        decalsParent.name = "Decals";
    }

    public void ClearSpawnedDecals()
    {
        foreach(GameObject go in CreatedDecals)
        {
            Destroy(go);
        }

        CreatedDecals.Clear();
    }

    public void SpawnDecal(Vector3 pos, string name)
    {
        Decal selected = null;

        for(int i = 0; i < AvailableDecals.Count; i++)
        {
            if(AvailableDecals[i].name == name)
            {
                DecalGroup group = AvailableDecals[i];
                selected = group.decals[Random.Range(0, group.decals.Length - 1)];
            }
        }

        if (selected != null)
        {
            SpawnAtPosition(pos, selected);
        }
        else
        {
            Debug.Log("Can't find decal named <b>" + name + "</b>!");
            return;
        }
    }

    public void SpawnDecal(Vector3 pos, int id)
    {
        if (id != -1)
        {
            Decal selected = null;

            for (int i = 0; i < AvailableDecals.Count; i++)
            {
                if (AvailableDecals[i].id == id)
                {
                    DecalGroup group = AvailableDecals[i];
                    selected = group.decals[Random.Range(0, group.decals.Length - 1)];
                }
            }

            if (selected != null)
            {
                SpawnAtPosition(pos, selected);
            }
            else
            {
                Debug.Log("Can't find decal with id <b>" + id + "</b>!");
                return;
            }
        }
    }

    void SpawnAtPosition(Vector3 pos, Decal dec)
    {
        GameObject newDecal = Instantiate<GameObject>(decalPrefab);
        newDecal.transform.SetParent(decalsParent, false);
        newDecal.transform.position = pos;
        float random = Random.Range(0.5f, 1f);
        newDecal.transform.localScale = new Vector3(random, random, 1);
        newDecal.GetComponent<SpriteRenderer>().sprite = dec.sprite;
        newDecal.GetComponent<SpriteRenderer>().sortingOrder = CreatedDecals.Count;
        newDecal.name = dec.name;

        CreatedDecals.Add(newDecal);
    }
}

[System.Serializable]
public class Decal
{
    public int id;
    public string name;
    public Sprite sprite;

    public Decal(int i, string n, Sprite s)
    {
        id = i;
        name = n;
        sprite = s;
    }
}

[System.Serializable]
public class DecalGroup
{
    public int id;
    public string name;
    public Decal[] decals;

    public DecalGroup(int i, string n, Decal[] d)
    {
        id = i;
        name = n;
        decals = d;
    }
}