﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(GameManager))]
public class MenuManager : MonoBehaviour
{
    public Transform anyInputText;

    public List<GameObject> Menus = new List<GameObject>();
    public Transform canvas;

    public int currentMenu = -1;
    public int previousMenu = -1;

    void Update()
    {
        WaitForInput();
    }

    void WaitForInput()
    {
        if (anyInputText != null && anyInputText.gameObject.activeSelf)
        {
            if (Input.anyKeyDown)
            {
                GameManager.instance.cursorVisible = true;
                ChangeMenu(0);
                ToggleAnyInput(false);
            }
        }
    } 

    public void ToggleAnyInput(bool b)
    {
        anyInputText.gameObject.SetActive(b);
    }

    public void ChangeMenu(int id)
    {
        if (id != currentMenu)
        {
            previousMenu = currentMenu;
            currentMenu = id;
            
            if (previousMenu != -1 && previousMenu != id)
            {
                Menus[previousMenu].GetComponent<Animator>().SetTrigger("Out");
            }

            Menus[currentMenu].GetComponent<Animator>().SetTrigger("In");
        }
    }

    public void HideMenu()
    {
        if (currentMenu != -1)
        {
            Menus[currentMenu].GetComponent<Animator>().SetTrigger("Out");
            currentMenu = -1;
        }
    }

    public void PreviousMenu()
    {
        ChangeMenu(previousMenu);
    }

    public void EnableMenus()
    {
        canvas.gameObject.SetActive(true);
    }

    public void DisableMenus()
    {
        HideMenu();
        canvas.gameObject.SetActive(false);
    }
}
