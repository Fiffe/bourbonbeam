﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour
{
    private Transform holder;
    private Transform healthFill;
    private Transform manaFill;
    private Transform staminaFill;
    private Image expFill;

    private CharacterStats stats;

    public ParticleSystem expEffect;
    public AudioSource expSound;

    float mappedHp = 1;
    float mappedMp = 1;
    float mappedSt = 1;
    float mappedExp = 0;

    private int prevLvl;

    void Awake()
    {
        holder = transform.FindChild("Metabolism").GetChild(0);
        healthFill = holder.FindChild("Health").GetChild(0);
        manaFill = holder.FindChild("Mana").GetChild(0);
        staminaFill = holder.FindChild("Stamina").GetChild(0);
        expFill = holder.FindChild("Exp").GetChild(0).GetComponent<Image>();
        expEffect = expFill.transform.parent.GetComponentInChildren<ParticleSystem>();
        expSound = expEffect.transform.parent.GetComponent<AudioSource>();
    }

    void Update()
    {
        if (stats)
        {
            HandleHealthbar();
            HandleLevels();
        }
        else
        {
            stats = GameManager.instance.myPlayer.GetComponent<CharacterStats>();
            prevLvl = stats.level;
        }
    }

    void HandleLevels()
    {
        if(prevLvl != stats.level)
        {
            expEffect.Play();
            expSound.Play();
            prevLvl = stats.level;
        }
    }

    void HandleHealthbar()
    {
        if (holder != null)
        {
            mappedHp = Mathf.Lerp(mappedHp, stats.health / stats.maxHealth, Time.deltaTime * 8f);
            mappedHp = Mathf.Clamp01(mappedHp);

            mappedMp = Mathf.Lerp(mappedMp, stats.mana / stats.maxMana, Time.deltaTime * 8f);
            mappedMp = Mathf.Clamp01(mappedMp);

            mappedSt = Mathf.Lerp(mappedSt, stats.stamina / stats.maxStamina, Time.deltaTime * 8f);
            mappedSt = Mathf.Clamp01(mappedSt);

            mappedExp = Mathf.Lerp(mappedExp, stats.exp / stats.reqExp, Time.deltaTime * 5f);
            mappedExp = Mathf.Clamp01(mappedExp);

            healthFill.localScale = new Vector3(mappedHp, 1, 1);
            manaFill.localScale = new Vector3(mappedMp, 1, 1);
            staminaFill.localScale = new Vector3(mappedSt, 1, 1);
            expFill.fillAmount = mappedExp;
        }
    }
}
