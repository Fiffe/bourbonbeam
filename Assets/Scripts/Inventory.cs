﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public List<Item> Items = new List<Item>();
    public List<GameObject> Slots = new List<GameObject>();

    private ItemDatabase itemDb;

    private PlayerController player;
    private Transform inventoryVis;
    private GameObject itemPrefab;
    private Transform itemsParent;
    private Tooltip tooltip;
    private ItemTrash trash;
    private GameObject lootVis;
    private Transform lootParent;
    private CharacterPanel chP;

    public bool opened = true;

    public int goldAmount = 0;

    public int occupiedSlots = 0;
    public int slotsCount = 20;

    public Lootable lootable;
    public bool lootOpened;

    public List<Item> Loot = new List<Item>();

    public Text invSpace;
    public Text goldTxt;
    
    void Awake()
    {
        itemDb = GameManager.instance.itemDb;
        player = GetComponent<PlayerController>();
        inventoryVis = GameManager.instance.HUD.transform.FindChild("Inventory");
        inventoryVis.FindChild("Layout").FindChild("Inv").FindChild("CloseBtn").GetComponent<Button>().onClick.AddListener(() => CloseInventory());
        itemPrefab = Resources.Load<GameObject>("Prefabs/Other/Item");
        itemsParent = inventoryVis.FindChild("Layout").FindChild("Inv").FindChild("Items").GetChild(0);
        tooltip = inventoryVis.GetComponentInChildren<Tooltip>();
        trash = inventoryVis.GetComponentInChildren<ItemTrash>();
        lootVis = inventoryVis.FindChild("Layout").FindChild("Loot").gameObject;
        lootVis.transform.FindChild("CloseBtn").GetComponent<Button>().onClick.AddListener(() => CloseInventory());
        lootParent = lootVis.transform.FindChild("Items").GetChild(0);
        invSpace = inventoryVis.FindChild("Layout").FindChild("Inv").FindChild("Space").GetComponentInChildren<Text>();
        goldTxt = inventoryVis.FindChild("Layout").FindChild("Inv").FindChild("Gold").GetComponentInChildren<Text>();
        chP = GetComponent<CharacterPanel>();

        ClearInventory();
        CloseLoot();

        GameManager.instance.myInv = this;
    }

    void Start()
    {
        CreateInventory();

        AddItem(1);
        ToggleInventory(false);
    }

    void Update()
    {
        ControlInput();
        HandleTextDisplay();
    }

    void CreateInventory()
    {
        for(int i = 0; i < slotsCount; i++)
        {
            Items.Add(new Item());
        }
    }

    void HandleTextDisplay()
    {
        if (invSpace)
        {
            invSpace.text = "Space: " + occupiedSlots + " <b>/</b> " + slotsCount;
        }

        if(goldTxt)
        {
            goldTxt.text = "GOLD: <color=\"#FFCD00FF\">" + goldAmount.ToString() + "</color>";
        }
    }

    void ControlInput()
    {
        if (Input.GetButtonDown("Inventory") && !GameManager.instance.igMenu.activated && !GameManager.instance.myChP.opened)
        {
            ToggleInventory(!opened);
        }

        if(Input.GetButtonDown("Cancel") && opened)
        {
            ToggleInventory(false);
        }
    }

    public void AddItem(int id)
    {
        for(int i = 0; i < Items.Count; i++)
        {
            if(Items[i].itemId == -1)
            {
                Item itToSpawn = itemDb.GetItemById(id);
                GameObject it = Instantiate(itemPrefab, itemsParent, false) as GameObject;

                if (itToSpawn.itemType != EItemType.Weapon)
                {
                    it.AddComponent<ItemSlot>();
                }
                else
                {
                    it.AddComponent<ItemSlotEquipable>();
                }

                it.GetComponent<Image>().overrideSprite = itToSpawn.sprite;
                it.GetComponent<Outline>().effectColor = RarityColors.GetRarityColor(itToSpawn.itemRarity);
                it.GetComponent<ItemSlot>().tp = tooltip;
                it.GetComponent<ItemSlot>().it = itToSpawn;
                it.GetComponent<ItemSlot>().slotId = i;

                if (itToSpawn.itemAmount <= 1)
                {
                    it.GetComponentInChildren<Text>().enabled = false;
                }
                else
                {
                    it.GetComponentInChildren<Text>().enabled = true;
                    it.GetComponentInChildren<Text>().text = itToSpawn.itemAmount.ToString();
                    it.GetComponentInChildren<Shadow>().effectColor = RarityColors.GetRarityColor(itToSpawn.itemRarity);
                }

                Items[i] = itToSpawn;
                Slots.Add(it);
                occupiedSlots++;
                return;
            }
        }
    }

    public void AddItem(int id, int count)
    {
        for (int i = 0; i < Items.Count; i++)
        {
            if (Items[i].itemId == -1)
            {
                Item itToSpawn = itemDb.GetItemById(id);
                GameObject it = Instantiate(itemPrefab, itemsParent, false) as GameObject;

                if (itToSpawn.itemType != EItemType.Weapon)
                {
                    it.AddComponent<ItemSlot>();
                }
                else
                {
                    it.AddComponent<ItemSlotEquipable>();
                }

                it.GetComponent<Image>().overrideSprite = itToSpawn.sprite;
                it.GetComponent<Outline>().effectColor = RarityColors.GetRarityColor(itToSpawn.itemRarity);
                it.GetComponent<ItemSlot>().tp = tooltip;
                it.GetComponent<ItemSlot>().it = itToSpawn;
                it.GetComponent<ItemSlot>().slotId = i;

                if (itToSpawn.itemAmount <= 1)
                {
                    it.GetComponentInChildren<Text>().enabled = false;
                }
                else
                {
                    it.GetComponentInChildren<Text>().enabled = true;
                    it.GetComponentInChildren<Text>().text = itToSpawn.itemAmount.ToString();
                    it.GetComponentInChildren<Shadow>().effectColor = RarityColors.GetRarityColor(itToSpawn.itemRarity);
                }

                Items[i] = itToSpawn;
                Slots.Add(it);
                occupiedSlots += count;
                return;
            }
        }
    }

    void ToggleInventory(bool b)
    {
        opened = b;
        GameManager.instance.cursorVisible = b;

        tooltip.Deactivate();

        if (!b)
        {
            trash.OnCloseInv();
        }

        if(!b)
        {
            if(lootOpened)
            {
                CloseLoot();
            }
        }

        inventoryVis.gameObject.SetActive(b);
    }

    public void CloseInventory()
    {
        ToggleInventory(false);
    }

    public void OpenLoot(Lootable loot)
    {
        lootVis.SetActive(true);
        lootable = loot;

        for(int i = 0; i < loot.Loot.Count; i++)
        {
            GameObject itVis = Instantiate(itemPrefab, lootParent, false) as GameObject;
            itVis.AddComponent<ItemSlot>();
            ItemSlot slot = itVis.GetComponent<ItemSlot>();

            Item it = itemDb.GetItemById(loot.Loot[i].itemId);

            itVis.GetComponent<Image>().overrideSprite = it.sprite;
            itVis.GetComponent<Outline>().effectColor = RarityColors.GetRarityColor(it.itemRarity);
            slot.tp = tooltip;
            slot.it = it;
            slot.amount = loot.Loot[i].amount;
            slot.loot = true;
            slot.slotId = i;
            slot.lootIt = loot.Loot[i];

            if (slot.amount <= 1)
            {
                itVis.GetComponentInChildren<Text>().enabled = false;
            }
            else
            {
                itVis.GetComponentInChildren<Text>().enabled = true;
                itVis.GetComponentInChildren<Text>().text = slot.amount.ToString();
                itVis.GetComponentInChildren<Shadow>().effectColor = RarityColors.GetRarityColor(it.itemRarity);
            }

            Loot.Add(it);
        }

        lootOpened = true;
        ToggleInventory(true);
    }

    public void CloseLoot()
    {
        Loot.Clear();

        foreach (Transform tr in lootParent)
        {            
            Destroy(tr.gameObject);
        }

        lootVis.SetActive(false);
    }

    public int EquipItem(ItemSlot islot)
    {
        if(islot.it.itemType == EItemType.Weapon)
        {
            Weapon wep = (Weapon)islot.it;

            if (chP.hand.heldIt.itemId == -1)
            {
                chP.hand.heldIt = islot.it;
                chP.hand.invId = islot.slotId;
                chP.hand.ToggleItemVis(true, islot.it);

                GameObject wepScriptGO = Instantiate(Resources.Load<GameObject>("Prefabs/Items/Weapons/" + islot.it.itemSlug));
                wepScriptGO.name = islot.it.itemSlug;
                wepScriptGO.transform.SetParent(player.weaponGO, false);
                wepScriptGO.transform.localPosition = Vector3.zero;
                
                if(wep.wepType == EWeaponType.Slashing)
                {
                    WeaponSlashing wepScript = wepScriptGO.GetComponent<WeaponSlashing>();

                    wepScript.angle = wep.angle;

                    player.wep = wepScript;

                    player.wepRend.transform.localPosition = wepScript.equipPos;
                    player.wepRend.transform.localRotation = Quaternion.Euler(wepScript.equipRot);
                }

                player.wepRend.sprite = wep.sprite;

                return 0;
            }
        }
        return -1;
    }

    public void DeequipItem(int id, int slot)
    {
        switch(slot)
        {
            case 0:
                chP.hand.heldIt = new Item();
                chP.hand.invId = -1;
                chP.hand.ToggleItemVis(false, new Item());
                break;
            case 1:
                chP.offhand.heldIt = new Item();
                chP.offhand.invId = -1;
                chP.offhand.ToggleItemVis(false, new Item());
                break;
        }

        if(id != -1)
        {
            for(int i = 0; i < Slots.Count; i++)
            {
                if (i == id)
                {
                    if(Slots[i].GetComponent<ItemSlotEquipable>())
                    {
                        Slots[i].GetComponent<ItemSlotEquipable>().ToggleEquip(false);
                    }
                }
            }
        }

        Destroy(player.wep.gameObject);
        player.wep = null;
        player.wepRend.sprite = null;
    }

    void ClearInventory()
    {
        foreach(Transform tr in itemsParent)
        {
            Destroy(tr.gameObject);

        }
    }
}