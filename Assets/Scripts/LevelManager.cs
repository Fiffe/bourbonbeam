﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(GameManager))]
public class LevelManager : MonoBehaviour
{
    private MenuManager menuMgr;

    public List<Level> Levels = new List<Level>();

    public int curLvlId = -1;

    public bool busy;

    void Start()
    {
        menuMgr = GameManager.instance.menuMgr;
    }

    public void LoadLevel(int i)
    {
        if (!busy)
        {
            StartCoroutine("LoadLevelAsync", i);
        }
    }

    IEnumerator LoadLevelAsync(int lvl)
    {
        busy = true;

        GameManager.instance.ReadyHUD();
        GameManager.instance.decMan.ClearSpawnedDecals();

        if (curLvlId != -1)
        {
            SceneManager.UnloadScene(curLvlId);
        }

        AsyncOperation async = SceneManager.LoadSceneAsync(lvl, LoadSceneMode.Additive);

        while (!async.isDone)
        {
            yield return async;
        }

        curLvlId = GetLevel(lvl).id;

        menuMgr.DisableMenus();
        menuMgr.currentMenu = -1;
        GameManager.instance.cursorVisible = false;
        GameManager.instance.cursorLocked = true;
        GameManager.instance.HUD.SetActive(true);
        busy = false;
    }

    public void BackToMenu()
    {
        if (curLvlId != -1)
        {
            SceneManager.UnloadScene(curLvlId);
        }

        GameManager.instance.igMenu.ToggleMenu(false);
        GameManager.instance.HUD.SetActive(false);

        GameManager.instance.cursorVisible = true;

        menuMgr.EnableMenus();
        menuMgr.HideMenu();
        menuMgr.ChangeMenu(0);
    }

    public Level GetLevel(int id)
    {
        for (int i = 0; i < Levels.Count; i++)
        {
            if (Levels[i].id == id)
            {
                return Levels[i];
            }
        }

        return null;
    }
}

[System.Serializable]
public class Level
{
    public int id;
    public string name;
    public Vector3 startPos;
}
