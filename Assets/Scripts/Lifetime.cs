﻿using UnityEngine;
using System.Collections;

public class Lifetime : MonoBehaviour
{
    public float lifetime;

    void Start()
    {
        StartCoroutine("DestroyMyself");
    }

    IEnumerator DestroyMyself()
    {
        yield return new WaitForSeconds(lifetime);
        Destroy(gameObject);
    }
}
