﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryUI : MonoBehaviour, IDropHandler
{
    public Transform content;

    private Inventory inv;

    void Awake()
    {
        content = transform.GetChild(0);
        inv = GameManager.instance.myInv;
    }

    public void OnDrop(PointerEventData data)
    {   
        if(!inv)
        {
            inv = GameManager.instance.myInv;
        }

        if (data.pointerDrag.GetComponent<ItemSlot>())  
        {
            
            ItemSlot dragged = data.pointerDrag.GetComponent<ItemSlot>();

            Item itToAdd = dragged.it;

            if (itToAdd.itemType != EItemType.Gold)
            {
                if (inv.slotsCount - inv.occupiedSlots > 0)
                {
                    inv.AddItem(itToAdd.itemId);
                    if (dragged.loot)
                    {
                        inv.lootable.Loot.Remove(dragged.lootIt);
                    }
                    Destroy(dragged.gameObject);
                }
            }
            else
            {
                inv.goldAmount += dragged.amount;
                inv.lootable.Loot.Remove(dragged.lootIt);
                Destroy(dragged.gameObject);
            }            
        }
    }
}
