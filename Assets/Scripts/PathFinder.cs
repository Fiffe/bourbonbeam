﻿using UnityEngine;
using System.Collections;

public class PathFinder : MonoBehaviour
{
    private StatesManager states;

    public Transform target;
    public float speed = 4;

    Vector2[] path;
    int targetIndex;

    void Start()
    {
        states = GetComponent<StatesManager>();
    }

    void Update()
    {
        if(states.isDead)
        {
            states.isWalking = false;
            target = null;
            StopAllCoroutines();
            enabled = false;
        }
    }

    public void FollowTarget()
    {
        StartCoroutine(RefreshPath());
    }

    public void StopFollowing()
    {
        states.isWalking = false;
        target = null;
        StopAllCoroutines();
    }

    IEnumerator RefreshPath()
    {
        if (target)
        {
            Vector2 targetPositionOld = (Vector2)target.position + Vector2.up; // ensure != to target.position initially

            while (true)
            {
                if (target && targetPositionOld != (Vector2)target.position)
                {
                    targetPositionOld = (Vector2)target.position;

                    path = Pathfinding.RequestPath(transform.position, target.position);
                    StopCoroutine("FollowPath");
                    StartCoroutine("FollowPath");
                }

                yield return new WaitForSeconds(.25f);
            }
        }
    }

    IEnumerator FollowPath()
    {
        if (target)
        {
            if (path.Length > 0)
            {
                targetIndex = 0;
                Vector2 currentWaypoint = path[0];

                while (true)
                {
                    if ((Vector2)transform.position == currentWaypoint)
                    {
                        targetIndex++;
                        if (targetIndex >= path.Length)
                        {
                            yield break;
                        }
                        currentWaypoint = path[targetIndex];
                    }

                    transform.position = Vector2.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
                    states.isWalking = true;

                    yield return null;

                }
            }
        }
    }

    public void OnDrawGizmos()
    {
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.black;
                //Gizmos.DrawCube((Vector3)path[i], Vector3.one *.5f);

                if (i == targetIndex)
                {
                    Gizmos.DrawLine(transform.position, path[i]);
                }
                else
                {
                    Gizmos.DrawLine(path[i - 1], path[i]);
                }
            }
        }
    }
}