﻿using UnityEngine;
using System.Collections;

public class SpriteOrderFix : MonoBehaviour
{
    private SpriteRenderer sprite;
    private StatesManager states;

    public float delay = .75f;

    void Awake()
    {
        states = GetComponentInParent<StatesManager>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        SwitchLayer();
        StartCoroutine("Sort", delay);
    }

    void Update()
    {
        if(states != null && states.isDead)
        {
            enabled = false;
        }
    }
    
    void SwitchLayer()
    {
        if (sprite)
        {
            sprite.sortingOrder = (int)Camera.main.WorldToScreenPoint(sprite.bounds.min).y * -1;
        }
    }

    IEnumerator Sort(float d)
    {
        while (true)
        {
            yield return new WaitForSeconds(d);
            SwitchLayer();
        }
    }
}
