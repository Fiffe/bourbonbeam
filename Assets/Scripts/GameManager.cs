﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public CameraMovement camMov;
    public MenuManager menuMgr;
    public LevelManager levelMgr;
    public IngameMenu igMenu;
    public ItemDatabase itemDb;
    public LootTables lootDb;
    public DecalManager decMan;

    public GameObject HUD;
    public GameObject myPlayer;
    public PlayerController myPc;
    public Inventory myInv;
    public CharacterPanel myChP;

    public bool cursorVisible;
    public bool cursorLocked;

    public GameObject hudPrefab;

    [Header("Debug")]
    public bool debug;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        levelMgr = GetComponent<LevelManager>();
        menuMgr = GetComponent<MenuManager>();
        itemDb = GetComponent<ItemDatabase>();
        lootDb = GetComponent<LootTables>();
        decMan = GetComponent<DecalManager>();
    }

    void Start()
    {
        if (!debug)
        {
            StartCoroutine("FirstLoad");
        }
    }

    void Update()
    {
        HandleCursor();
        HandleTimeScale();
    }

    void HandleCursor()
    {
        Cursor.visible = cursorVisible;

        if (myPc)
        {
            myPc.crossRend.enabled = !cursorVisible;
        }

        if (cursorLocked)
        {
            Cursor.lockState = CursorLockMode.Confined;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }

    void HandleTimeScale()
    {
        if (igMenu && myInv)
        {
            if (igMenu.activated)
            {
                Time.timeScale = 0;
            }
            else if (!igMenu.activated)
            {
                Time.timeScale = 1;
            }
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void ReadyHUD()
    {
        if(HUD != null)
        {
            Destroy(HUD);
        }

        GameObject hud = Instantiate(hudPrefab);
        HUD = hud;
        HUD.GetComponent<Canvas>().worldCamera = Camera.main;
        igMenu = HUD.GetComponentInChildren<IngameMenu>();
        HUD.SetActive(false);
    }

    public void ShakeScreen(float a, float t)
    {
        if(camMov)
        {
            camMov.Shake(a, t);
        }
    }

    IEnumerator FirstLoad()
    {
        yield return new WaitForSeconds(1);
        menuMgr.ToggleAnyInput(true);
    }

    public void CloseApp()
    {
        if (!Application.isEditor)
        {
            Application.Quit();
        }
    }
}
