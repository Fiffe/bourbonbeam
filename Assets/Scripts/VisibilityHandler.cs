﻿using UnityEngine;
using System.Collections.Generic;

public class VisibilityHandler : MonoBehaviour
{
    public bool visible;

    public List<SpriteRenderer> Renderers = new List<SpriteRenderer>();

    private StatesManager states;

    private bool switched;

    void Start()
    {
        states = GetComponent<StatesManager>();
        switched = !visible;

        if(Renderers.Count == 0)
        {
            enabled = false;
        }
    }

    void Update()
    {
        if(states.isDead && !visible)
        {
            visible = true;
        }

        if(visible && !switched)
        {
            Switch(true);
        }
        else if(!visible && switched)
        {
            Switch(false);
        }
    }

    void Switch(bool b)
    {
        foreach(SpriteRenderer rend in Renderers)
        {
            rend.enabled = b;
        }

        switched = b;
    }
}
