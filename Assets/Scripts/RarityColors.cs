﻿using UnityEngine;
using System.Collections;

public class RarityColors
{
    public static Color32 GetRarityColor(EItemRarity rarity)
    {
        switch (rarity)
        {
            default: return new Color32(128, 128, 128, 128);
            case EItemRarity.Common:
                return new Color32(128, 128, 128, 128);
            case EItemRarity.Rare:
                return new Color32(66, 98, 255, 128);
            case EItemRarity.Legendary:
                return new Color32(255, 234, 66, 128);
            case EItemRarity.Mythical:
                return new Color32(255, 69, 0, 128);
        }
    }
}