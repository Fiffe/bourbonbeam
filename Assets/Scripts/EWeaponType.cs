﻿public enum EWeaponType
{
    Slashing,
    Piercing,
    Single,
    Ranged
}