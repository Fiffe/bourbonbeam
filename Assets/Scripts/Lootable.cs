﻿using UnityEngine;
using System.Collections.Generic;

public class Lootable : MonoBehaviour
{
    public List<LootableItem> Loot = new List<LootableItem>();

    public GameObject effect;

    public int tableId;

    protected ItemDatabase itemDb;
    protected LootTables lootDb;

    public void Awake()
    {
        itemDb = GameManager.instance.itemDb;
        lootDb = GameManager.instance.lootDb;
        effect = transform.FindChild("Effects").FindChild("Loot").gameObject;
        ToggleEffect(false);
    }

    public void Update()
    {
        if(Loot.Count == 0 && effect.activeSelf)
        {
            ToggleEffect(false);
        }
    }

    protected void RollItems()
    {
        LootTable table = lootDb.GetTableById(tableId);
        for (int i = 0; i < table.items.Length; i++)
        {
            float target = table.items[i].rate;
            float roll = Random.Range(0f, 1f);
            roll = Mathf.Round(roll * 100f) / 100f;

            if(roll <= target)
            {
                int amt = Random.Range(table.items[i].minAmt, table.items[i].maxAmt);
                LootableItem it = new LootableItem(table.items[i].id, amt);
                Loot.Add(it);
            }
        }
    }

    protected virtual void ToggleEffect(bool b)
    {
        if (GetComponent<BoxCollider2D>().isTrigger && b)
        {
            GetComponent<BoxCollider2D>().isTrigger = false;
        }
        effect.SetActive(b);
    }
}

public class LootableItem
{
    public int itemId;
    public int amount;

    public LootableItem(int id, int amt)
    {
        itemId = id;
        amount = amt;
    }
}
