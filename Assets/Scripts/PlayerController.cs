﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;
    private BoxCollider2D terCol;
    private CircleCollider2D charCol;
    private Animator anim;
    private Transform crosshair;
    private SpriteRenderer rend;
    private Transform fovTr;
    private FieldOfView fov;
    private CameraMovement camMov;
    private CharacterStats stats;
    private Inventory inv;

    public StatesManager states;

    private float movSpeed;

    public float basicMovSpeed;
    public float movSpeedMultiplier = 1;

    private float horizontal;
    private float vertical;

    private bool isWalking;

    private float mouseX;
    private float mouseY;

    public float mouseSens = 0.5f;

    private Vector2 lookDir;

    public Transform weaponGO;
    public SpriteRenderer wepRend;
    public WeaponBase wep;

    public SpriteRenderer crossRend;

    private GameObject lastHitLoot;
    private LayerMask lootMask;

    public Text interactionText;

    public Vector3 crosshairPos
    {
        get
        {
            return crosshair.position;
        }
    }

    public bool isMoving
    {
        get
        {
            return isWalking;
        }
    }

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        terCol = GetComponent<BoxCollider2D>();
        charCol = GetComponentInChildren<CircleCollider2D>();
        states = GetComponent<StatesManager>();
        fov = GetComponentInChildren<FieldOfView>();
        crosshair = transform.FindChild("Crosshair");
        rend = transform.FindChild("Sprite").GetComponentInChildren<SpriteRenderer>();
        anim = rend.transform.parent.GetComponent<Animator>();
        weaponGO = transform.FindChild("Weapon");
        wepRend = weaponGO.FindChild("Hand").FindChild("Sprite").GetComponent<SpriteRenderer>();
        fovTr = fov.transform.parent;
        camMov = Camera.main.GetComponent<CameraMovement>();
        stats = GetComponent<CharacterStats>();
        crossRend = crosshair.GetComponent<SpriteRenderer>();

        interactionText = GameManager.instance.HUD.transform.FindChild("TooltipMsg").GetComponent<Text>();
        DeactivateTooltip();

        inv = GetComponent<Inventory>();

        GameManager.instance.myPlayer = gameObject;
        GameManager.instance.myPc = this;

        movSpeed = basicMovSpeed;

        lootMask = (1 << LayerMask.NameToLayer("Enemy") | 1 << LayerMask.NameToLayer("Friendly") | 1 << LayerMask.NameToLayer("Props"));
    }

    void Update()
    {
        if(states.isHit)
        {
            GameManager.instance.ShakeScreen(0.125f, .1f);
        }

        if (!states.isDead)
        {
            HandleAnimator();
            AimWeapon();
            if (Time.timeScale != 0 && !inv.opened)
            {
                ControlWeapon();
                ControlLooting();
            }
        }
    }

    void FixedUpdate()
    {
        ControlMovement();
        HandleCrosshair();
    }

    void ControlMovement()
    {
        movSpeed = basicMovSpeed * movSpeedMultiplier;

        if (states.canWalk && !states.isDead && !inv.opened)
        {
            horizontal = Input.GetAxis("Horizontal");
            vertical = Input.GetAxis("Vertical");
        }
        else
        {
            horizontal = 0;
            vertical = 0;
        }

        rb.velocity = new Vector2(Mathf.Lerp(0, horizontal * movSpeed, 0.8f), Mathf.Lerp(0, vertical * movSpeed, 0.8f));
    }

    void HandleAnimator()
    {
        lookDir = (transform.position - crosshair.position).normalized;
        if (lookDir.x < 0)
        {
            rend.transform.parent.localScale = new Vector3(-1, 1, 1);
        }
        else if (lookDir.x > 0)
        {
            rend.transform.parent.localScale = new Vector3(1, 1, 1);
        }

        if ((Input.GetButton("Horizontal") || Input.GetButton("Vertical")) && states.canWalk)
        {
            isWalking = true;
        }
        else
        {
            isWalking = false;
        }

        if (isWalking)
        {
            anim.speed = 1 * movSpeedMultiplier;
        }
        else
        {
            anim.speed = 1;
        }

        states.isWalking = this.isWalking;
    }

    void HandleCrosshair()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        pos = new Vector3(pos.x, pos.y, 10);

        crosshair.position = pos;
        
        if (!states.stagger && !states.isDead)
        {
            Vector2 dir = fovTr.position - crosshair.position;
            dir = dir.normalized;
            float rotY = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
            fovTr.rotation = Quaternion.Euler(0, 0, -rotY);
            fov.transform.rotation = Quaternion.Euler(0, -rotY - 90, 0);
            if (wep != null)
            {
                if (wep.GetComponent<WeaponSlashing>())
                {
                    WeaponSlashing wp = (WeaponSlashing)wep;
                    wp.transform.rotation = Quaternion.Euler(0, rotY + 180, 0);
                }
            }
        }
    }

    void AimWeapon()
    {
        if (weaponGO != null && !states.stagger)
        {
            Vector2 dir = transform.position - crosshair.position;
            dir = dir.normalized;
            float rotZ = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
            weaponGO.rotation = Quaternion.Euler(0, 0, -rotZ);
        }
    }

    void ControlWeapon()
    {
        if (wep != null && states.canAttack)
        {
            if (Input.GetButton("Fire1"))
            {
                if (!wep.justUsed)
                {
                    wep.Use();
                }
            }
            
            if(Input.GetButton("Fire2"))
            {
                if(!wep.justUsed)
                {
                    stats.UseStamina(0);
                    wep.secondary = true;
                    wep.Secondary();
                }
            }
            
            if(Input.GetButtonUp("Fire2"))
            {
                if(wep.secondary)
                {
                    wep.secondary = false;
                    wep.isBlocking = false;
                }
            }

            states.isBlocking = wep.isBlocking;
        }
    }

    void ControlLooting()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 10f, lootMask);

        if (hit.collider)
        {
            GameObject hitObj = hit.collider.gameObject;

            if (hitObj != lastHitLoot)
            {
                SetLastHitObjIllum(0);
                DeactivateTooltip();
            }

            if (Vector2.Distance(hitObj.transform.position, transform.position) < 1.5f)
            {
                if (hitObj.GetComponent<Lootable>())
                {
                    Lootable loot = hitObj.GetComponent<Lootable>();
                    if (loot.Loot.Count > 0)
                    {
                        lastHitLoot = hitObj;
                        SetLastHitObjIllum(0.45f);
                        ActivateTooltip(InteractionMessages.lootMsg);

                        Lootable lt = lastHitLoot.GetComponent<Lootable>();
                        if (Input.GetButtonDown("Interact"))
                        {
                            inv.OpenLoot(lt);
                        }
                    }
                    else
                    {
                        SetLastHitObjIllum(0f);
                        DeactivateTooltip();
                    }
                }
            }
            else
            {
                SetLastHitObjIllum(0);
                DeactivateTooltip();
            }
        }
        else
        {
            SetLastHitObjIllum(0);
            DeactivateTooltip();
        }

        if(interactionText.IsActive() && GameManager.instance.cursorVisible)
        {
            DeactivateTooltip();
        }
    }

    void SetLastHitObjIllum(float v)
    {
        if (lastHitLoot != null)
        {
            lastHitLoot.transform.FindChild("Sprite").FindChild("Base").GetComponent<SpriteRenderer>().material.SetFloat("_FlashAmount", v);
            lastHitLoot.transform.FindChild("Sprite").FindChild("Base").GetComponent<SpriteRenderer>().material.SetFloat("_SelfIllum", v);
        }
    }

    void ActivateTooltip(string s)
    {
        interactionText.text = s;
        interactionText.gameObject.SetActive(true);
    }

    void DeactivateTooltip()
    {
        interactionText.gameObject.SetActive(false);
    }
}
