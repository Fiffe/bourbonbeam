﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ItemTrash : MonoBehaviour, IDropHandler
{
    public GameObject itToDelete;

    public GameObject confWindow;

    private Inventory inv;

    void Awake()
    {
        inv = GameManager.instance.myInv;
        confWindow = transform.parent.FindChild("Confirmation").gameObject;
        ToggleConfirmation(false);
    }

    public void OnCloseInv()
    {
        ReturnItem();

        itToDelete = null;
        ToggleConfirmation(false);
    }

    public void OnDrop(PointerEventData data)
    {
        if (data.pointerDrag.GetComponent<ItemSlot>())
        {
            ItemSlot it = data.pointerDrag.GetComponent<ItemSlot>();

            if (it.canDrag)
            {
                itToDelete = it.gameObject;
                it.canDrag = false;
            }

            it.transform.SetParent(this.transform);
            ToggleConfirmation(true);
        }
    }

    void ToggleConfirmation(bool b)
    {
        confWindow.SetActive(b);
    }

    public void GetConfirmation(bool b)
    {
        if(b)
        {
            RemoveItem();
        }
        else
        {
            ReturnItem();
        }

        itToDelete = null;
        ToggleConfirmation(false);
    }

    void ReturnItem()
    {
        if (itToDelete != null)
        {
            ItemSlot it = itToDelete.GetComponent<ItemSlot>();
            itToDelete.transform.SetParent(it.content);
            it.canDrag = true;
            it.img.raycastTarget = true;
        }
    }

    void RemoveItem()
    {
        int index = itToDelete.GetComponent<ItemSlot>().slotId;
        if (itToDelete.GetComponent<ItemSlot>().loot)
        {
            inv.Loot[index] = new Item();
            inv.lootable.Loot.RemoveAt(index);
        }

        Destroy(itToDelete);
    }
}
