﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    protected PathFinder pathfinding;
    protected Animator anim;
    protected CharacterStats stats;
    protected Rigidbody2D rb;
    protected BoxCollider2D terCol;
    protected SpriteRenderer baseSprite;
    protected CircleCollider2D charCol;
    protected StatesManager states;

    public Transform weaponGO;
    public WeaponBase wep;

    public Transform target;

    protected Vector2 lookDir;

    protected bool isWalking;

    public float basicMovSpeed;
    protected float movSpeed;

    public float sightRadius;
    public LayerMask sightMask;

    public float followDistance;

    public bool debug;

    void Start()
    {
        pathfinding = GetComponent<PathFinder>();
        stats = GetComponent<CharacterStats>();
        baseSprite = transform.FindChild("Sprite").GetComponentInChildren<SpriteRenderer>();
        anim = baseSprite.transform.parent.GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        terCol = GetComponent<BoxCollider2D>();
        charCol = GetComponentInChildren<CircleCollider2D>();
        weaponGO = transform.FindChild("Weapon");
        wep = weaponGO.GetComponentInChildren<WeaponBase>();
        states = GetComponent<StatesManager>();

        sightMask = (1 << LayerMask.NameToLayer("Friendly"));
    }

    void Update()
    {
        if (!states.isDead)
        {
            FindTarget();
            LookAtTarget();
            HandleWeaponRot();
            MoveToEnemy();
            ControlWeapon();

            HandleAnimator();
        }
        else
        {
            enabled = false;
        }
    }

    void HandleWeaponRot()
    {
        if(target && !states.stagger)
        {
            Vector2 lDir = lookDir.normalized;
            float rotZ = Mathf.Atan2(lDir.x, lDir.y) * Mathf.Rad2Deg;
            weaponGO.rotation = Quaternion.Euler(0, 0, -rotZ + 35);

            if (wep != null)
            {
                if (wep.GetComponent<WeaponSlashing>())
                {
                    float rotY = Mathf.Atan2(lDir.x, lDir.y) * Mathf.Rad2Deg;

                    WeaponSlashing wp = (WeaponSlashing)wep;
                    wp.transform.rotation = Quaternion.Euler(0, rotY + 180, 0);
                }
            }
        }
        else
        {
            weaponGO.rotation = Quaternion.identity;
        }
    }

    void ControlWeapon()
    {
        if(target && !states.stagger)
        {
            if (wep != null)
            {
                if(Vector2.Distance(target.position, transform.position) < wep.wepIt.range + Random.Range(0f, 1f))
                {
                    if (!wep.justUsed)
                    {
                        wep.Use();
                    }
                }
            }
        }        
    }

    void FindTarget()
    {
        if (!target)
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, sightRadius, sightMask);

            foreach (Collider2D col in cols)
            {
                if (col.GetComponent<PlayerController>())
                {
                    PlayerController pl = col.GetComponent<PlayerController>();

                    if (!pl.states.isDead)
                    {
                        target = pl.transform;
                    }
                }
            }
        }
    }

    void LookAtTarget()
    {
        if(target && !states.stagger)
        {
            lookDir = transform.position - target.position;

            if(Vector2.Distance(transform.position, target.position) > followDistance || target.GetComponent<StatesManager>().isDead)
            {
                target = null;
            }
        }
    }

    void HandleAnimator()
    {
        movSpeed = basicMovSpeed;
        pathfinding.speed = movSpeed;

        if (lookDir.x < 0)
        {
            baseSprite.transform.parent.localScale = new Vector3(-1, 1, 1);
        }
        else if (lookDir.x > 0)
        {
            baseSprite.transform.parent.localScale = new Vector3(1, 1, 1);
        }

        isWalking = states.isWalking;

        anim.SetBool("Walking", isWalking);
    }

    void MoveToEnemy()
    {
        if (target && !states.stagger)
        {
            if (Vector2.Distance(transform.position, target.position) > wep.wepIt.range)
            {
                pathfinding.target = target;
                if (!states.isWalking)
                {
                    pathfinding.FollowTarget();
                }
            }
            else
            {
                pathfinding.StopFollowing();
            }
        }
        else
        {
            pathfinding.StopFollowing();
        }
    }


    void OnDrawGizmos()
    {
        if (debug)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, sightRadius);
        }
    }
}
